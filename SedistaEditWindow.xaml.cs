﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaUpisIzmenaAerodroma.xaml
    /// </summary>
    public partial class SedistaEditWindow : Window
    {
        string upisIzmena;
        string sifra;

        public SedistaEditWindow(string upisIzmena, string sifra)
        {
            InitializeComponent();

            this.upisIzmena = upisIzmena;
            this.sifra = sifra;

            


            if (upisIzmena == "upis")
            {
                lbSifra.Content = "Sifra:";
            }
            else if (upisIzmena == "izmena")
            {

                foreach (Sedista s in Podaci.lstSedistaa)
                {

                    if (s.sifra == sifra)
                    {

                        //lbSifra.Content = a.sifra;
                        tbSifra.Text = s.sifra;
                        tbX.Text = s.x;
                        tbY.Text = s.y;

                        tbSifra.IsEnabled = false;

                    }
                }

                btnDodaj.Content = "Izmeni";
            }

        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {

            string sifra = tbSifra.Text;
            string x = tbX.Text;
            string y = tbY.Text;

            if (upisIzmena == "upis")
            {
                bool postojiSifra = false;
                foreach (Sedista s in Podaci.lstSedistaa)
                    if (s.sifra == sifra)
                        postojiSifra = false;

                if (!postojiSifra)
                {
                    Sedista s = new Sedista(sifra, x, y, false);
                    Podaci.lstSedistaa.Add(s);
                    this.Close();
                }
                else
                    MessageBox.Show("Uneta sifra vec postoji!");

            }
            else if (upisIzmena == "izmena")
            {

                for (int i = 0; i < Podaci.lstSedistaa.Count; i++)
                {

                    if (Podaci.lstSedistaa.ElementAt(i).sifra == sifra)
                    {

                        Podaci.lstSedistaa.ElementAt(i).y = x;
                        Podaci.lstSedistaa.ElementAt(i).y = y;

                        this.Close();
                    }
                }
            }


        }
    }
}
