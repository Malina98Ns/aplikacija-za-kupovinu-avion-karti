﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaUpisIzmenaLeta.xaml
    /// </summary>
    public partial class LetEditWindow : Window
    {

        string upisIzmena;
        int brojLeta;
        public LetEditWindow(string upisIzmena, int brojLeta)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.brojLeta = brojLeta;

            inicijalizujVremena();
            inicijalizujAerodrome();


            if (upisIzmena == "upis")
            {
                lbBrojLeta.Content = "Broj leta: " + Podaci.generisiBrojLeta();
            }
            else if (upisIzmena == "izmena") {

                foreach (Let l in Podaci.lstLetovi) {

                    if (l.brojLeta == brojLeta) {

                        lbBrojLeta.Content = "Broj leta: " + brojLeta;

                        dpDatumPolaska.SelectedDate = l.datumPolaska;
                        cbVremePolaskaSati.Text = l.vremePolaska.Substring(0, 2);
                        cbVremePolaskaMinuti.Text = l.vremePolaska.Substring(3, 2);

                        dpDatumDolaska.SelectedDate = l.datumDolaska;
                        cbVremeDolaskaSati.Text = l.vremeDolaska.Substring(0, 2);
                        cbVremeDolaskaMinuti.Text = l.vremeDolaska.Substring(3, 2);

                        tbCenaKarte.Text = l.cenaKarte.ToString();

                        lbNazivAerodromaPolazak.Content = l.aerodromPolazak;
                        cbSifraAerodromaPolazak.Text = l.sifraAerodromaPolazak;
                        lbNazivAerodromaOdrediste.Content = l.aerodromOdrediste;
                        cbSifraAerodromaOdrediste.Text = l.sifraAerodromaOdrediste;

                    }
                   
                }

                btnDodaj.Content = "Izmeni";
            }
        }
        private void inicijalizujVremena() {

            for (int i = 0; i <= 23; i++) {

                if (i < 10)
                {
                    cbVremePolaskaSati.Items.Add("0" + i);
                    cbVremeDolaskaSati.Items.Add("0" + i);
                }
                else
                {
                    cbVremePolaskaSati.Items.Add(i);
                    cbVremeDolaskaSati.Items.Add(i);
                }
            }
            for (int i = 0; i <= 59; i++)
            {

                if (i < 10)
                {
                    cbVremePolaskaMinuti.Items.Add("0" + i);
                    cbVremeDolaskaMinuti.Items.Add("0" + i);
                }
                else
                {
                    cbVremePolaskaMinuti.Items.Add(i);
                    cbVremeDolaskaMinuti.Items.Add(i);
                }
            }

            cbVremePolaskaSati.SelectedIndex = 12;
            cbVremePolaskaMinuti.SelectedIndex = 0;

            cbVremeDolaskaSati.SelectedIndex = 18;
            cbVremeDolaskaMinuti.SelectedIndex = 30;
        }
        private void inicijalizujAerodrome() {


            foreach (Aerodrom a in Podaci.lstAerodromi)
            {
                cbSifraAerodromaPolazak.Items.Add(a.sifra);
                cbSifraAerodromaOdrediste.Items.Add(a.sifra);
            }

            cbSifraAerodromaPolazak.SelectedIndex = 0;
            cbSifraAerodromaOdrediste.SelectedIndex = 1;

            if (cbSifraAerodromaPolazak.Items.Count > 0 && cbSifraAerodromaOdrediste.Items.Count > 0) {

                foreach (Aerodrom a in Podaci.lstAerodromi)
                {
                    if (a.sifra == cbSifraAerodromaPolazak.SelectedItem.ToString())
                        lbNazivAerodromaPolazak.Content = a.nazivAerodroma;
                    if (a.sifra == cbSifraAerodromaOdrediste.SelectedItem.ToString())
                        lbNazivAerodromaOdrediste.Content = a.nazivAerodroma;
                }
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void cbSifraAerodromaPolazak_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbSifraAerodromaPolazak.Items.Count > 0)
            {
                foreach (Aerodrom a in Podaci.lstAerodromi)
                    if (a.sifra == cbSifraAerodromaPolazak.SelectedItem.ToString())
                        lbNazivAerodromaPolazak.Content = a.nazivAerodroma;
            }
        }

        private void cbSifraAerodromaOdrediste_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbSifraAerodromaOdrediste.Items.Count > 0)
            {
                foreach (Aerodrom a in Podaci.lstAerodromi)
                    if (a.sifra == cbSifraAerodromaOdrediste.SelectedItem.ToString())
                        lbNazivAerodromaOdrediste.Content = a.nazivAerodroma;
            }
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            
            DateTime datumPolaska=dpDatumPolaska.SelectedDate.Value;
            string vremePolaska=cbVremePolaskaSati.SelectedItem.ToString()+":"+cbVremePolaskaMinuti.SelectedItem.ToString();
            DateTime datumDolaska=dpDatumDolaska.SelectedDate.Value;
            string vremeDolaska= cbVremeDolaskaSati.SelectedItem.ToString() + ":" + cbVremeDolaskaMinuti.SelectedItem.ToString();
            double cenaKarte=Convert.ToDouble(tbCenaKarte.Text);
            string aerodromPolazak=lbNazivAerodromaPolazak.Content.ToString();
            string sifraAerodromaPolazak=cbSifraAerodromaPolazak.SelectedItem.ToString();
            string aerodromOdrediste = lbNazivAerodromaOdrediste.Content.ToString();
            string sifraAerodromaOdrediste=cbSifraAerodromaOdrediste.SelectedItem.ToString();

            if (upisIzmena == "upis")
            {
                if (cbSifraAerodromaPolazak.SelectedItem.ToString() != cbSifraAerodromaOdrediste.SelectedItem.ToString())
                {
                    Let l = new Let(Podaci.generisiBrojLeta(), datumPolaska, vremePolaska, datumDolaska, vremeDolaska,
                        cenaKarte, aerodromPolazak, sifraAerodromaPolazak, aerodromOdrediste, sifraAerodromaOdrediste);
                    Podaci.lstLetovi.Add(l);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Odabrali ste isti aerodrom za polazak i odrediste!");
                }
            }
            else if (upisIzmena == "izmena") {

                if (cbSifraAerodromaPolazak.SelectedItem.ToString() != cbSifraAerodromaOdrediste.SelectedItem.ToString())
                {
                    for (int i= 0; i< Podaci.lstLetovi.Count; i++) {

                        if (Podaci.lstLetovi.ElementAt(i).brojLeta==brojLeta) {

                            Podaci.lstLetovi.ElementAt(i).datumPolaska = datumPolaska;
                            Podaci.lstLetovi.ElementAt(i).vremePolaska = vremePolaska;
                            Podaci.lstLetovi.ElementAt(i).datumDolaska = datumDolaska;
                            Podaci.lstLetovi.ElementAt(i).vremeDolaska = vremeDolaska;
                            Podaci.lstLetovi.ElementAt(i).cenaKarte = cenaKarte;
                            Podaci.lstLetovi.ElementAt(i).aerodromPolazak = aerodromPolazak;
                            Podaci.lstLetovi.ElementAt(i).sifraAerodromaPolazak = sifraAerodromaPolazak;
                            Podaci.lstLetovi.ElementAt(i).aerodromOdrediste = aerodromOdrediste;
                            Podaci.lstLetovi.ElementAt(i).sifraAerodromaOdrediste = sifraAerodromaOdrediste;

                            this.Close();

                        }


                    }
                }
                else
                {
                    MessageBox.Show("Odabrali ste isti aerodrom za polazak i odrediste!");
                }

            }
        }
    }
}
