﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaAvioni.xaml
    /// </summary>
    public partial class AvionWindow : Window
    {
        private CollectionViewSource cvs;
        public AvionWindow()
        {
            InitializeComponent();
            dataGridAvioni.ItemsSource = Podaci.lstAvioni;
            cvs = new CollectionViewSource();
            cvs.Source = Podaci.lstAvioni;
            dataGridAvioni.ItemsSource = cvs.View;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AvionEditWindow aew = new AvionEditWindow("upis", 0);
            aew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAvioni.SelectedIndex != -1 && Podaci.lstAvioni.Count > 0)
            {

                Avion a = (Avion)dataGridAvioni.SelectedItem;

                AvionEditWindow aew = new AvionEditWindow("izmena", a.sifra);
                aew.ShowDialog();

                dataGridAvioni.Items.Refresh();

            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAvioni.SelectedIndex != -1 && Podaci.lstAvioni.Count > 0)
            {

                Avion a = (Avion)dataGridAvioni.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true)
                {
                    for (int i = 0; i < Podaci.lstAvioni.Count; i++)
                    {
                        if (Podaci.lstAvioni.ElementAt(i).sifra == a.sifra)
                            Podaci.lstAvioni.ElementAt(i).obrisan = true;
                    }
                }

                dataGridAvioni.Items.Refresh();
            }
        }
        private void tbPretragaBrLeta_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterBrLeta);
        }

        private void tbPretragaNazivaKompanije_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterNazivKompanije);
        }
        private void MyFilterBrLeta(object sender, FilterEventArgs e)
        {
            Avion a = e.Item as Avion;
            if (a != null)
            {
                e.Accepted = a.brojLeta.ToString().Contains(tbPretragaPoBrLeta.Text.ToLower());
            }
        }

        private void MyFilterNazivKompanije(object sender, FilterEventArgs e)
        {
            Avion a = e.Item as Avion;
            if (a != null)
            {
                e.Accepted = a.sifraAvioKompanije.ToString().Contains(tbPretragaPoAvioKompaniji.Text.ToLower());
            }
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
