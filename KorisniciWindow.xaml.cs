﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    
    public partial class KorisniciWindow : Window
    {
        private CollectionViewSource cvs;
        public KorisniciWindow()
        {
            InitializeComponent();
            dataGridKorisnici.ItemsSource = Podaci.lstKorisnici;
            cvs = new CollectionViewSource();
            cvs.Source = Podaci.lstKorisnici;
            dataGridKorisnici.ItemsSource = cvs.View;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow rw = new RegistracijaWindow("upis", "");
            rw.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridKorisnici.SelectedIndex != -1 && Podaci.lstKorisnici.Count > 0)
            {

                Korisnik k = (Korisnik)dataGridKorisnici.SelectedItem;

                RegistracijaWindow rw = new RegistracijaWindow("izmena", k.korisnickoIme);
                rw.ShowDialog();

                dataGridKorisnici.Items.Refresh();

            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridKorisnici.SelectedIndex != -1 && Podaci.lstKorisnici.Count > 0)
            {

                Korisnik k = (Korisnik)dataGridKorisnici.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true)
                {
                    
                   // Podaci.lstKorisnici.Remove(dataGridKorisnici.SelectedItem as Korisnik);
                    
                    for (int i = 0; i < Podaci.lstKorisnici.Count; i++)
                    {
                        if (Podaci.lstKorisnici.ElementAt(i).korisnickoIme == k.korisnickoIme)
                            Podaci.lstKorisnici.ElementAt(i).obrisan = true;
                            
                    }
                }

                dataGridKorisnici.Items.Refresh();
            }
        }

        private void tbPretragaImena_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterIme);
        }
        
        private void tbPretragaPrezimena_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterPrezime);
        }

        private void tbPretragaKorImena_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterKorIme);
        }
        private void tbPretragaTipaKorisnika_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterTipKorisnika);
        }


        private void MyFilterIme(object sender, FilterEventArgs e)
        {
            Korisnik k = e.Item as Korisnik;
            if (k != null)
            {
                e.Accepted = k.ime.ToLower().Contains(tbPretragaPoImenu.Text.ToLower());
            }
        }

        private void MyFilterPrezime(object sender, FilterEventArgs e)
        {
            Korisnik k = e.Item as Korisnik;
            if (k != null)
            {
                e.Accepted = k.prezime.ToLower().Contains(tbPretragaPoPrezimenu.Text.ToLower());
            }
        }
        private void MyFilterKorIme(object sender, FilterEventArgs e)
        {
            Korisnik k = e.Item as Korisnik;
            if (k != null)
            {
                e.Accepted = k.korisnickoIme.ToLower().Contains(tbPretragaPoKorImenu.Text.ToLower());
            }
        }
        private void MyFilterTipKorisnika(object sender, FilterEventArgs e)
        {
            Korisnik k = e.Item as Korisnik;
            if (k != null)
            {
                e.Accepted = k.tipKorisnika.ToLower().Contains(tbPretragaPoTipuKor.Text.ToLower());
            }
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
