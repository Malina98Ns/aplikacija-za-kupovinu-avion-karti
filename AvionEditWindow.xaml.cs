﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaUpisIzmenaAviona.xaml
    /// </summary>
    public partial class AvionEditWindow : Window
    {

        string upisIzmena;
        int sifraAviona;
        public AvionEditWindow(string upisIzmena, int sifraAviona)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.sifraAviona = sifraAviona;

            if (Podaci.lstAvioKompanije.Count > 0)
            {
                foreach (AvioKompanija ak in Podaci.lstAvioKompanije)
                    cbAvioKompanije.Items.Add(ak.sifra);

                cbAvioKompanije.SelectedIndex = 0;
            }
            if (Podaci.lstLetovi.Count > 0)
            {
                foreach (Let l in Podaci.lstLetovi)
                    cbBrojeviLetova.Items.Add(l.brojLeta);

                cbBrojeviLetova.SelectedIndex = 0;
            }

            if (upisIzmena == "upis")
            {
                lbSifra.Content = "Sifra: " + Podaci.generisiSifruAviona().ToString();
            }
            else if (upisIzmena == "izmena") {

                foreach (Avion a in Podaci.lstAvioni) {

                    if (a.sifra == sifraAviona)
                    {
                        lbSifra.Content = "Sifra: " + a.sifra.ToString();
                        cbAvioKompanije.Text = a.sifraAvioKompanije;
                        cbBrojeviLetova.Text = a.brojLeta.ToString();
                        tbPilot.Text = a.pilot;

                        if (a.lstSlobodnaSedista.Count > 0 && a.lstZauzetaSedista.Count > 0)
                        {
                            tbBrojRedova.Text = a.lstSlobodnaSedista.ElementAt(0).x.ToString();
                            tbBrojKolona.Text = a.lstSlobodnaSedista.ElementAt(0).y.ToString();
                        }

                        tbBrojRedovaBiznisKlasa.Text = a.brRedovaBiznisKlasa.ToString();
                    }
                }

                btnDodaj.Content = "Izmeni";
            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            //validacija
            
            string sifraAvioKompanije = cbAvioKompanije.SelectedItem.ToString();
            int brojLeta = Convert.ToInt32(cbBrojeviLetova.SelectedItem.ToString());

            int brojRedova = Convert.ToInt32(tbBrojRedova.Text);
            int brojKolona = Convert.ToInt32(tbBrojKolona.Text);

            string pilot = tbPilot.Text;
            int brojRedovaBiznisKlasa = Convert.ToInt32(tbBrojRedovaBiznisKlasa.Text);

            //(int sifra, int brojLeta, string sifraAvioKompanije, string pilot, int brRedovaBiznisKlasa)
            if (upisIzmena == "upis") {

                int sifra = Podaci.generisiSifruAviona();
                Avion a = new Avion(sifra, brojLeta, sifraAvioKompanije, pilot, brojRedovaBiznisKlasa);
                string klasaSedista = "biznis";

                for (int i = 1; i <= brojRedova; i++) {
                    for (int j = 1; j <= brojKolona; j++) {
                        if (i > brojRedovaBiznisKlasa)
                            klasaSedista = "ekonomska";

                        Sediste s = new Sediste(Podaci.generisiSifruSedista(), sifra, i, j, klasaSedista, true);
                        a.lstSlobodnaSedista.Add(s);
                    }
                }



                Podaci.lstAvioni.Add(a);
                this.Close();

            } else if (upisIzmena=="izmena") {

                for (int i = 0; i < Podaci.lstAvioni.Count; i++) {

                    if (Podaci.lstAvioni.ElementAt(i).sifra == sifraAviona) {

                        Podaci.lstAvioni.ElementAt(i).brojLeta = brojLeta;
                        Podaci.lstAvioni.ElementAt(i).sifraAvioKompanije = sifraAvioKompanije;
                        Podaci.lstAvioni.ElementAt(i).pilot = pilot;
                        Podaci.lstAvioni.ElementAt(i).brRedovaBiznisKlasa = brojRedovaBiznisKlasa;

                        this.Close();
                    }
                }
            }
        }
    }
}
