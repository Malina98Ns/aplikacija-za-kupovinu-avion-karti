﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    
    public partial class AerodromWindow : Window
    {
        public AerodromWindow()
        {
            InitializeComponent();

            dataGridAerodromi.ItemsSource = Podaci.lstAerodromi;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AerodromEditWindow frm = new AerodromEditWindow("upis", "");
            frm.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAerodromi.SelectedIndex != -1 && Podaci.lstAerodromi.Count>0) {

                Aerodrom a = (Aerodrom)dataGridAerodromi.SelectedItem;

                AerodromEditWindow frm = new AerodromEditWindow("izmena", a.sifra);
                frm.ShowDialog();

                dataGridAerodromi.Items.Refresh();

            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAerodromi.SelectedIndex != -1 && Podaci.lstAerodromi.Count > 0)
            {

                Aerodrom a = (Aerodrom)dataGridAerodromi.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true) {
                    for (int i = 0; i < Podaci.lstAerodromi.Count; i++) {
                        if (Podaci.lstAerodromi.ElementAt(i).sifra == a.sifra)
                            Podaci.lstAerodromi.ElementAt(i).obrisan = true;
                            //Podaci.lstAerodromi.RemoveAt(i);
                    }
                }

                dataGridAerodromi.Items.Refresh();

            }
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
