﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Projekat
{
    
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
            InitializeComponent();

            Podaci.inicijalizujPodatke();

        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPrijavaRegistrovanKorisnik_Click(object sender, RoutedEventArgs e)
        {
            string korisnickoIme = tbKorisnickoIme.Text;
            string lozinka = pbLozinka.Password;

            bool postojiKorisnik = false;
            foreach (Korisnik k in Podaci.lstKorisnici)
                if (k.korisnickoIme == korisnickoIme && k.lozinka == lozinka)
                {
                    postojiKorisnik = true;
                    //MessageBox.Show("postoji korisnik");
                }

            if (!postojiKorisnik) {
                MessageBox.Show("Uneli ste nepostojece korisnicko ime i/ili sifru!");

            }else if(korisnickoIme=="" || lozinka == ""){
                MessageBox.Show("Morate uneti korisnicko ime i lozinku!");
            }else{
                foreach (Korisnik k in Podaci.lstKorisnici)
                    if (k.korisnickoIme == korisnickoIme && k.lozinka == lozinka)
                    {
                        if (k.tipKorisnika == "administrator") {
                            tbKorisnickoIme.Text = "";
                            pbLozinka.Password = "";
                            AdministratorWindow aw = new AdministratorWindow();
                            aw.ShowDialog();
                        }
                        if (k.tipKorisnika == "putnik") {
                            tbKorisnickoIme.Text = "";
                            pbLozinka.Password = "";
                            PutnikWindow frm = new PutnikWindow(k.korisnickoIme);
                            frm.ShowDialog();
                        }
                    }

            }

            //FormaKorisnika frm = new FormaKorisnika();
            //frm.ShowDialog();
        }

        private void BtnPrijavaNeregistrovanKorisnik_Click(object sender, RoutedEventArgs e)
        {
            NeregistrovanKorisnikWindow nkw = new NeregistrovanKorisnikWindow();
            nkw.ShowDialog();
        }

        private void BtnRegistracija_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow rw = new RegistracijaWindow("upis", "");
            rw.ShowDialog();
        }
        private void miOProgramu_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("AirMalina v1.0\nUradio: Nikola Malinovic", "O programu", MessageBoxButton.OK);
        }
    }
}
