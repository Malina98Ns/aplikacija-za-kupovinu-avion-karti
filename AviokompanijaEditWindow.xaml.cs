﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaUpisIzmenaAvioKompanija.xaml
    /// </summary>
    public partial class AviokompanijaEditWindow : Window
    {
        string upisPrikaz;
        public AviokompanijaEditWindow(string upisPrikaz, string sifra)
        {
            InitializeComponent();
            this.upisPrikaz = upisPrikaz;

            if (upisPrikaz == "prikaz") {
                tbSifra.Text = sifra;
                btnDodaj.Visibility = Visibility.Hidden;
                tbSifra.IsEnabled = false;

                foreach (AvioKompanija ak in Podaci.lstAvioKompanije)
                    if (ak.sifra == tbSifra.Text)
                        dataGrid.ItemsSource = ak.lstLetovi;

            }
        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            if (tbSifra.Text != "") {
                string sifra = tbSifra.Text;

                bool postojiSifra = false;
                foreach (AvioKompanija ak in Podaci.lstAvioKompanije) {
                    if (ak.sifra == sifra){
                        postojiSifra = true;
                        
                    }
                }
                if (!postojiSifra)
                {
                    AvioKompanija avioKomp = new AvioKompanija(sifra);
                    Podaci.lstAvioKompanije.Add(avioKomp);

                    this.Close();
                }
                else {
                    MessageBox.Show("Uneta sifra vec postoji!");
                }
            }
        }


    }
}
