﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaUpisIzmenaAerodroma.xaml
    /// </summary>
    public partial class AerodromEditWindow : Window
    {
        string upisIzmena;
        string sifra;

        public AerodromEditWindow(string upisIzmena, string sifra)
        {
            InitializeComponent();

            this.upisIzmena = upisIzmena;
            this.sifra = sifra;

            foreach (string grad in Podaci.lstGradovi)
                cbGradovi.Items.Add(grad);
            cbGradovi.SelectedIndex = 0;


            if (upisIzmena == "upis")
            {
                lbSifra.Content = "Sifra:";
            }
            else if (upisIzmena == "izmena") {

                foreach (Aerodrom a in Podaci.lstAerodromi) {

                    if (a.sifra == sifra) {

                        //lbSifra.Content = a.sifra;
                        tbSifra.Text = a.sifra;
                        tbNazivAerodroma.Text = a.nazivAerodroma;
                        cbGradovi.SelectedItem = a.nazivGrada;

                        tbSifra.IsEnabled = false;

                    }
                }

                btnDodaj.Content = "Izmeni";
            }

        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {

            string sifraAerodroma = tbSifra.Text;
            string nazivAerodroma = tbNazivAerodroma.Text;
            string nazivGrada = cbGradovi.SelectedItem.ToString();

            if (upisIzmena == "upis")
            {
                bool postojiSifra = false;
                foreach (Aerodrom ae in Podaci.lstAerodromi)
                    if (ae.sifra == sifraAerodroma)
                        postojiSifra = true;

                if (!postojiSifra)
                {
                    Aerodrom a = new Aerodrom(sifraAerodroma, nazivAerodroma, nazivGrada, false);
                    Podaci.lstAerodromi.Add(a);
                    this.Close();
                }
                else
                    MessageBox.Show("Uneta sifra aerodroma vec postoji!");

            }
            else if (upisIzmena == "izmena") {

                for (int i = 0; i < Podaci.lstAerodromi.Count; i++) {

                    if (Podaci.lstAerodromi.ElementAt(i).sifra == sifra) {

                        Podaci.lstAerodromi.ElementAt(i).nazivAerodroma = nazivAerodroma;
                        Podaci.lstAerodromi.ElementAt(i).nazivGrada = nazivGrada;

                        this.Close();
                    }
                }
            }

            
        }
    }
}
