﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaPutnika.xaml
    /// </summary>
    public partial class PutnikWindow : Window
    {
        string korisnickoIme;
        public PutnikWindow(string korisnickoIme)
        {
            InitializeComponent();
            this.korisnickoIme = korisnickoIme;

            dataGrid.ItemsSource = Podaci.lstLetovi;
        }

        private void BtnIzmenaProfila_Click(object sender, RoutedEventArgs e)
        {
            RegistracijaWindow rw = new RegistracijaWindow("izmena", korisnickoIme);
            rw.ShowDialog();
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
