﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    
    public partial class KartaEditWindow : Window
    {
        string upisIzmena;
        string brLeta;

        public KartaEditWindow(string upisIzmena, string brLeta)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.brLeta = brLeta;

            cbKlasaSedista.Items.Add("ekonomska");
            cbKlasaSedista.Items.Add("biznis");
            cbKlasaSedista.SelectedIndex = 0;

            

            if (upisIzmena == "izmena")
            {

                /*broj leta, broj sedišta, naziv putnika 
                (korisničko ime ako je ulogovan korisnik, ime i prezime ako se korisnik nije ulogovao dok je kupovao kartu), 
                klasa sedišta (ekonomska i biznis), kapija, ukupna cena karte. */
                foreach (Karta k in Podaci.lstKarta)
                    if (k.brLeta == brLeta)
                    {
                        tbBrLeta.Text = k.brLeta;
                        tbBrSedista.Text = k.brSedista;
                        tbNazivPutnika.Text = k.nazivPutnika;
                        tbKapija.Text = k.kapija;
                        tbCenaKarte.Text = k.cenaKarte;
                        cbKlasaSedista.SelectedItem = k.klasaSedista;
                    }

                btnRegistrujSe.Content = "Izmeni";
            }

        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnDodaj_Click(object sender, RoutedEventArgs e)
        {
            //public Korisnik(string ime, string prezime, string email, string adresaStanovanja, string pol, 
            //string korisnickoIme, string lozinka, string tipKorisnika)

            string brLeta = tbBrLeta.Text;
            string brSedista = tbBrSedista.Text;
            string nazivPutnika = tbNazivPutnika.Text;
            string kapija = tbKapija.Text;

            string cenaKarte = tbCenaKarte.Text;
            string klasaSedista = cbKlasaSedista.SelectedItem.ToString();

            if (upisIzmena == "upis")
            {
                if (!postojiKarta(brLeta))
                {

                    Karta k = new Karta(brLeta, brSedista, nazivPutnika, klasaSedista, kapija, cenaKarte,false);
                    Podaci.lstKarta.Add(k);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Uneto korisnicko ime vec postoji!");
                }
            }
            else if (upisIzmena == "izmena")
            {

                if (!postojiKarta(brLeta))
                {

                    for (int i = 0; i < Podaci.lstKarta.Count; i++)
                    {
                        if (Podaci.lstKarta.ElementAt(i).brLeta == brLeta)
                        {

                            Podaci.lstKarta.ElementAt(i).brLeta = brLeta;
                            Podaci.lstKarta.ElementAt(i).brSedista = brSedista;
                            Podaci.lstKarta.ElementAt(i).nazivPutnika = nazivPutnika;
                            Podaci.lstKarta.ElementAt(i).kapija = kapija;
                            Podaci.lstKarta.ElementAt(i).cenaKarte = cenaKarte;
                            Podaci.lstKarta.ElementAt(i).klasaSedista = klasaSedista;
                            

                            this.Close();

                        }

                    }
                }
                else
                {
                    MessageBox.Show("Uneto korisnicko ime vec postoji!");
                }
            }


        }

        private bool postojiKarta(string brLeta)
        {

            foreach (Karta k  in Podaci.lstKarta)
            {
                if (k.brLeta == brLeta && brLeta != this.brLeta)
                    //korisnik moze ostaviti korisnicko ime nepromenjeno a ono svakako postoji u listi
                    //treba pronaci da li u  listi postoji korisnicko ime prosledjeno s forme
                    //a da se razlikuje od korisnickog imena prosledjenog formi prilikom ucitavannja za izmenu
                    return true;
            }
            return false;
        }
    }
}
