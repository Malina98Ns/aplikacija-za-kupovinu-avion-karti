﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Avion
    {
        public int sifra { get; set; }
        public int brojLeta { get; set; }
        public string sifraAvioKompanije { get; set; }
        public string pilot { get; set; }

        public int brRedovaBiznisKlasa { get; set; }

        public bool obrisan { get; set; }

        public List<Sediste> lstSlobodnaSedista;
        public List<Sediste> lstZauzetaSedista;

        public Avion(int sifra, int brojLeta, string sifraAvioKompanije, string pilot, int brRedovaBiznisKlasa)
        {
            this.sifra = sifra;
            this.brojLeta = brojLeta;
            this.sifraAvioKompanije = sifraAvioKompanije;
            this.pilot = pilot;
            this.brRedovaBiznisKlasa = brRedovaBiznisKlasa;

            lstSlobodnaSedista = new List<Sediste>();
            lstZauzetaSedista = new List<Sediste>();
        }
    }
}
