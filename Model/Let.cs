﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Let
    {


        public int brojLeta { get; set; }
        public DateTime datumPolaska { get; set; }
        public string vremePolaska { get; set; }

        public DateTime datumDolaska { get; set; }
        public string vremeDolaska { get; set; }

        public double cenaKarte { get; set; }

        public string aerodromPolazak { get; set; }
        public string sifraAerodromaPolazak { get; set; }

        public string aerodromOdrediste { get; set; }

        public string sifraAerodromaOdrediste { get; set; }

        public bool obrisan { get; set; }


        public Let(int brojLeta, DateTime datumPolaska, string vremePolaska, DateTime datumDolaska, string vremeDolaska, double cenaKarte, string aerodromPolazak, string sifraAerodromaPolazak, string aerodromOdrediste, string sifraAerodromaOdrediste)
        {
            this.brojLeta = brojLeta;
            this.datumPolaska = datumPolaska;
            this.vremePolaska = vremePolaska;
            this.datumDolaska = datumDolaska;
            this.vremeDolaska = vremeDolaska;
            this.cenaKarte = cenaKarte;
            this.aerodromPolazak = aerodromPolazak;
            this.sifraAerodromaPolazak = sifraAerodromaPolazak;
            this.aerodromOdrediste = aerodromOdrediste;
            this.sifraAerodromaOdrediste = sifraAerodromaOdrediste;
        }


    }
}
