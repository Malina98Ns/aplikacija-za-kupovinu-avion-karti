﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Karta
    {

        public string brLeta { get; set; }
        public string brSedista { get; set; }
        public string nazivPutnika { get; set; }
        public string klasaSedista { get; set; }
        public string kapija { get; set; }
        public string cenaKarte { get; set; }
        public bool ponistena { get; set; }

        public Karta(string brLeta, string brSedista, string nazivPutnika, string klasaSedista, string kapija, string cenaKarte, bool ponistena)
        {
           this.brLeta = brLeta;
           this.brSedista = brSedista;
           this.nazivPutnika = nazivPutnika;
           this.klasaSedista = klasaSedista;
           this.kapija = kapija;
           this.cenaKarte = cenaKarte;
           this.ponistena = ponistena;
        }
        public string prikazPodataka()
        {
            return "Broj leta: " + brLeta + ", Broj sedista: " + brSedista + ", Naziv putnika : " + nazivPutnika + ", Klasa sedista: " +
                klasaSedista + ", Kapija: " + kapija + ", Cena karte: " + cenaKarte + ", Ponistena: " + ponistena;
        }
    }
}






/* broj leta, broj sedišta, naziv putnika (korisničko ime ako je ulogovan korisnik,
 * ime i prezime ako se korisnik nije ulogovao dok je kupovao kartu), klasa sedišta (ekonomska i biznis), kapija,
 * ukupna cena karte. Cena se povećava 50% ako je izabrana biznis klasa. Karta je uvek u jednom smeru. Ako putnik odabere povratan let kreira se odvojena karta. */
