﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Korisnik
    {


        //– ime, prezime, email, adresastanovanja, pol, korisničkoime (jedinstveno), lozinka, 
        //tip korisnika (korisnikaplikacijemože da bude administrator iliputnik).

        public string ime { get; set; }
        public string prezime { get; set; }
        public string email { get; set; }
        public string adresaStanovanja { get; set; }
        public string pol { get; set; }
        public string korisnickoIme { get; set; }
        public string lozinka { get; set; }
        public string tipKorisnika { get; set; }

        public bool obrisan { get; set; }

        public Korisnik(string ime, string prezime, string email, string adresaStanovanja, string pol, string korisnickoIme, string lozinka, string tipKorisnika)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.email = email;
            this.adresaStanovanja = adresaStanovanja;
            this.pol = pol;
            this.korisnickoIme = korisnickoIme;
            this.lozinka = lozinka;
            this.tipKorisnika = tipKorisnika;
        }
    }
}
