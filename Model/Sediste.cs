﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Sediste
    {


        public int sifra { get; set; }
        public int sifraAviona { get; set; }
        public int x { get; set; }
        public int y { get; set; }
        public string klasaSedista { get; set; }
        public bool dostupno { get; set; }

        public Sediste(int sifra, int sifraAviona, int x, int y, string klasaSedista, bool dostupno)
        {
            this.sifra = sifra;
            this.sifraAviona = sifraAviona;
            this.x = x;
            this.y = y;
            this.klasaSedista = klasaSedista;
            this.dostupno = dostupno;
        }


    }
}
