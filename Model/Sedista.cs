﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Sedista
    {
        public string sifra { get; set; }
        public string x { get; set; }
        public string y { get; set; }
        public bool zauzeto { get; set; }


        public Sedista(string sifra, string x, string y, bool zauzeto)
        {
            this.sifra = sifra;
            this.x = x;
            this.y = y;

            this.zauzeto = zauzeto;
        }
    }
}
