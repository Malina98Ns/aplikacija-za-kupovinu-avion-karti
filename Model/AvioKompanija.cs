﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class AvioKompanija
    {
        public string sifra { get; set; }
        public List<Let> lstLetovi;

        public bool obrisana { get; set; }

        public AvioKompanija(string sifra)
        {
            this.sifra = sifra;
            lstLetovi = new List<Let>();
        }

        public void dodajLet(Let let) {

            lstLetovi.Add(let);
        }


    }
}
