﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Aerodrom
    {

        public string sifra { get; set; }
        public string nazivAerodroma { get; set; }
        public string nazivGrada { get; set; }
        public bool obrisan { get; set; }


        public Aerodrom(string sifra, string nazivAerodroma, string nazivGrada, bool obrisan)
        {
            this.sifra = sifra;
            this.nazivAerodroma = nazivAerodroma;
            this.nazivGrada = nazivGrada;
            this.obrisan = obrisan;
        }

        public string prikazPodataka() {

            return "Sifra: " + sifra + ", Naziv aerodroma: " + nazivAerodroma + ", Naziv grada: " + nazivGrada + ", Obrisan: " + obrisan;
        }




    }
}
