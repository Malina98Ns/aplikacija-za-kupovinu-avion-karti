﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaAvioKompanije.xaml
    /// </summary>
    public partial class AviokompanijaWindow : Window
    {
        public AviokompanijaWindow()
        {
            InitializeComponent();

            dataGridAvioKompanije.ItemsSource = Podaci.lstAvioKompanije;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijaEditWindow aew = new AviokompanijaEditWindow("upis", "");
            aew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {

            if (dataGridAvioKompanije.SelectedIndex != -1 && Podaci.lstAvioKompanije.Count > 0)
            {

                AvioKompanija ak = (AvioKompanija)dataGridAvioKompanije.SelectedItem;
                AviokompanijaEditWindow aew = new AviokompanijaEditWindow("prikaz", ak.sifra);
                aew.ShowDialog();
            }
                
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridAvioKompanije.SelectedIndex != -1 && Podaci.lstAvioKompanije.Count > 0)
            {

                AvioKompanija ak = (AvioKompanija)dataGridAvioKompanije.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true)
                {
                    for (int i = 0; i < Podaci.lstAvioKompanije.Count; i++)
                    {
                        if (Podaci.lstAvioKompanije.ElementAt(i).sifra == ak.sifra)
                            Podaci.lstAvioKompanije.ElementAt(i).obrisana = true;
                    }
                }

                dataGridAvioKompanije.Items.Refresh();
            }
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
