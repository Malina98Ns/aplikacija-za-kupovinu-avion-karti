﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaLetovi.xaml
    /// </summary>
    public partial class LetWindow : Window
    {
        private CollectionViewSource cvs;
        public LetWindow()
        {
            InitializeComponent();
            dataGridLetovi.ItemsSource = Podaci.lstLetovi;
            cvs = new CollectionViewSource();
            cvs.Source = Podaci.lstLetovi;
            dataGridLetovi.ItemsSource = cvs.View;

            if (Podaci.lstAvioKompanije.Count > 0)
            {

                foreach (AvioKompanija ak in Podaci.lstAvioKompanije)
                    cbSifraAvioKompanije.Items.Add(ak.sifra);

            }
            if (cbSifraAvioKompanije.Items.Count > 0)
                cbSifraAvioKompanije.SelectedIndex = 0;
                
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            LetEditWindow lew = new LetEditWindow("upis",0);
            lew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridLetovi.SelectedIndex != -1 && Podaci.lstLetovi.Count > 0)
            {

                Let l = (Let)dataGridLetovi.SelectedItem;

                LetEditWindow lew = new LetEditWindow("izmena", l.brojLeta);
                lew.ShowDialog();

                dataGridLetovi.Items.Refresh();

            }
        }

        private void btnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridLetovi.SelectedIndex != -1 && Podaci.lstLetovi.Count > 0)
            {

                Let l = (Let)dataGridLetovi.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true)
                {
                    for (int i = 0; i < Podaci.lstLetovi.Count; i++)
                    {
                        if (Podaci.lstLetovi.ElementAt(i).brojLeta == l.brojLeta)
                            Podaci.lstLetovi.ElementAt(i).obrisan = true;
                    }
                }

                dataGridLetovi.Items.Refresh();
            }
        }

        private void btnDodeliSelektovanLet_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridLetovi.SelectedIndex != -1 && Podaci.lstLetovi.Count > 0)
            {

                Let l = (Let)dataGridLetovi.SelectedItem;

                bool vecDodeljenLet = false;
                foreach (AvioKompanija ak in Podaci.lstAvioKompanije)
                    foreach (Let let in ak.lstLetovi)
                        if (let.brojLeta == l.brojLeta)
                            vecDodeljenLet = true;

                if (!vecDodeljenLet && cbSifraAvioKompanije.Items.Count > 0)
                {

                    string sifraAvioKompanije = cbSifraAvioKompanije.SelectedItem.ToString();
                    for (int i = 0; i < Podaci.lstAvioKompanije.Count; i++)
                        if (Podaci.lstAvioKompanije.ElementAt(i).sifra == sifraAvioKompanije)
                        {
                            Podaci.lstAvioKompanije.ElementAt(i).dodajLet(l);
                            MessageBox.Show("Let  je uspesno dodeljen.");
                        }


                }
                else if (vecDodeljenLet)
                    MessageBox.Show("Odabrani let je vec dodeljen!");

               
            }
        }
        private void tbPretragaPoBrLeta_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterBrLeta);
        }

        private void tbPretragaCene_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterCena);
        }

        private void tbPretragaOdredista_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterOdredista);
        }
        private void tbPretragaDestinacije_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterDestinacija);
        }
        
        private void MyFilterBrLeta(object sender, FilterEventArgs e)
        {
            Let l = e.Item as Let;
            if (l != null)
            {
                e.Accepted = l.brojLeta.ToString().Contains(tbPretragaPoBrLeta.Text.ToLower());
            }
        }

        private void MyFilterCena(object sender, FilterEventArgs e)
        {
            Let l = e.Item as Let;
            if (l != null)
            {
                e.Accepted = l.cenaKarte.ToString().Contains(tbPretragaPoCeni.Text.ToLower());
            }
        }
        private void MyFilterOdredista(object sender, FilterEventArgs e)
        {
            Let l = e.Item as Let; 
            if (l != null)
            {
                e.Accepted = l.aerodromPolazak.ToLower().Contains(tbPretragaPoOdredistu.Text.ToLower());
            }
        }
        private void MyFilterDestinacija(object sender, FilterEventArgs e)
        {
            Let l = e.Item as Let;
            if (l != null)
            {
                e.Accepted = l.aerodromOdrediste.ToLower().Contains(tbPretragaPoDestinaciji.Text.ToLower());
            }
        }
        
            
        
        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
