﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekat
{
    public class Podaci
    {
        public static ObservableCollection<Aerodrom> lstAerodromi = new ObservableCollection<Aerodrom>();
        public static ObservableCollection<Korisnik> lstKorisnici = new ObservableCollection<Korisnik>();
        public static ObservableCollection<Let> lstLetovi = new ObservableCollection<Let>();
        public static ObservableCollection<AvioKompanija> lstAvioKompanije = new ObservableCollection<AvioKompanija>();
        public static ObservableCollection<Avion> lstAvioni = new ObservableCollection<Avion>();
        public static ObservableCollection<Karta> lstKarta = new ObservableCollection<Karta>();
        public static ObservableCollection<Sediste> lstSedista = new ObservableCollection<Sediste>();
        public static ObservableCollection<Sedista> lstSedistaa = new ObservableCollection<Sedista>();

        public static List<string> lstGradovi = new List<string>();

        public static void inicijalizujPodatke() {

            Aerodrom a1 = new Aerodrom("BG", "Nikola Tesla", "Beograd", false);
            Aerodrom a2 = new Aerodrom("NY", "John F Kennedy","New York",false);

            lstAerodromi.Add(a1);
            lstAerodromi.Add(a2);

            Korisnik k1 = new Korisnik("Petar", "Petrovic", "pera80@gmail.com", "Bulevar neznanog junaka bb", "muski", "perap", "123", "putnik");
            Korisnik k2 = new Korisnik("Marko", "Markovic", "markom90@gmail.com", "Bulevar Nikole Tesle bb", "muski", "admin", "admin", "administrator");
            lstKorisnici.Add(k1);
            lstKorisnici.Add(k2);

            //public Let(int brojLeta, DateTime datumPolaska, string vremePolaska, DateTime datumDolaska, 
            //string vremeDolaska, double cenaKarte, string aerodromPolazak, string sifraAerodromaPolazak, 
            //string aerodromOdrediste, string sifraAerodromaOdrediste)
            Let l1 = new Let(1, new DateTime(), "11:33", new DateTime(), "22:30", 45000, "Nikola Tesla", "BG", "John F Kennedy", "NY");
            lstLetovi.Add(l1);
            Let l2 = new Let(2, new DateTime(), "10:43", new DateTime(), "23:30", 34555, "John F Kennedy", "NY", "Nikola Tesla", "BG");
            lstLetovi.Add(l2);

            AvioKompanija ak1 = new AvioKompanija("Srpski avio prevoz");
            lstAvioKompanije.Add(ak1);

            lstGradovi.Add("Beograd");
            lstGradovi.Add("Bec");
            lstGradovi.Add("London");
            lstGradovi.Add("Amsterdam");
            lstGradovi.Add("Pariz");
            lstGradovi.Add("Zagreb");
        }

        public static int generisiBrojLeta() {

            int noviBroj = 0;
            foreach (Let l in lstLetovi)
                if (l.brojLeta > noviBroj)
                    noviBroj = l.brojLeta;

            noviBroj++;
            return noviBroj;
        }

        public static int generisiSifruAviona()
        {

            int novaSifra = 0;
            foreach (Avion a in lstAvioni)
                if (a.sifra > novaSifra)
                    novaSifra = a.sifra;

            novaSifra++;
            return novaSifra;
        }

        public static int generisiSifruSedista()
        {

            int novaSifra = 0;
            foreach (Sediste s in lstSedista)
                if (s.sifra > novaSifra)
                    novaSifra = s.sifra;

            novaSifra++;
            return novaSifra;
        }


    }
}
