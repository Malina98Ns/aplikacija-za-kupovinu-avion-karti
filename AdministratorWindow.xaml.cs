﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    
    public partial class AdministratorWindow : Window
    {
        public AdministratorWindow()
        {
            InitializeComponent();
        }

        private void BtnPrikaziAerodrome_Click(object sender, RoutedEventArgs e)
        {
            AerodromWindow aw = new AerodromWindow();
            aw.ShowDialog();
        }

        private void BtnPrikaziKorisnike_Click(object sender, RoutedEventArgs e)
        {
            KorisniciWindow kw = new KorisniciWindow();
            kw.ShowDialog();
        }

        private void BtnPrikazLetova_Click(object sender, RoutedEventArgs e)
        {
            LetWindow lw = new LetWindow();
            lw.ShowDialog();
        }

        private void BtnPrikazAvioKompanija_Click(object sender, RoutedEventArgs e)
        {
            AviokompanijaWindow aw = new AviokompanijaWindow();
            aw.ShowDialog();
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void BtnPrikaziAvione_Click(object sender, RoutedEventArgs e)
        {
            AvionWindow aw = new AvionWindow();
            aw.ShowDialog();
        }

        private void BtnPrikaziKarte_Click(object sender, RoutedEventArgs e)
        {
            KartaWindow kw = new KartaWindow();
            kw.ShowDialog();
        }
        private void BtnPrikaziSedista_Click(object sender, RoutedEventArgs e)
        {
            SedistaWindow sw = new SedistaWindow();
            sw.ShowDialog();
        }
    }
}
