﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaAvioKompanije.xaml
    /// </summary>
    public partial class SedistaWindow : Window
    {
        public SedistaWindow()
        {
            InitializeComponent();

            dataGridSedista.ItemsSource = Podaci.lstSedistaa;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            SedistaEditWindow sew = new SedistaEditWindow("upis", "");
            sew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {

            if (dataGridSedista.SelectedIndex != -1 && Podaci.lstSedistaa.Count > 0)
            {

                Sedista se = (Sedista)dataGridSedista.SelectedItem;
                SedistaEditWindow sew = new SedistaEditWindow("prikaz", se.sifra);
                sew.ShowDialog();
            }
                
        }

        private void BtnRezervisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridSedista.SelectedIndex != -1 && Podaci.lstSedistaa.Count > 0)
            {

                Sedista s = (Sedista)dataGridSedista.SelectedItem;

                PotvrdaRezervacijeWindow prw = new PotvrdaRezervacijeWindow();
                prw.ShowDialog();

                if (prw.DialogResult.Value == true)
                {
                    for (int i = 0; i < Podaci.lstSedistaa.Count; i++)
                    {
                        if (Podaci.lstSedistaa.ElementAt(i).sifra == s.sifra)
                            Podaci.lstSedistaa.ElementAt(i).zauzeto = true;
                    }
                }

                dataGridSedista.Items.Refresh();
            }
        }

        private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
