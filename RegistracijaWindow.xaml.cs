﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
    /// <summary>
    /// Interaction logic for FormaRegistracija.xaml
    /// </summary>
    public partial class RegistracijaWindow : Window
    {
        string upisIzmena;
        string korisnickoIme;

        public RegistracijaWindow(string upisIzmena, string korisnickoIme)
        {
            InitializeComponent();
            this.upisIzmena = upisIzmena;
            this.korisnickoIme = korisnickoIme;

            cbTipKorisnika.Items.Add("putnik");
            cbTipKorisnika.Items.Add("administrator");
            cbTipKorisnika.SelectedIndex = 0;

            rbZenski.IsChecked = true;

            if (upisIzmena == "izmena") {

                //public Korisnik(string ime, string prezime, string email, string adresaStanovanja, string pol, 
                //string korisnickoIme, string lozinka, string tipKorisnika)
                foreach (Korisnik k in Podaci.lstKorisnici)
                    if (k.korisnickoIme == korisnickoIme) {
                        tbIme.Text = k.ime;
                        tbPrezime.Text = k.prezime;
                        tbEmail.Text = k.email;
                        tbAdresaStanovanja.Text = k.adresaStanovanja;

                        if (k.pol == "muski")
                            rbMuski.IsChecked = true;
                        else
                            rbZenski.IsChecked = true;

                        tbKorisnickoIme.Text = k.korisnickoIme;
                        pbLozinka.Password = k.lozinka;
                        cbTipKorisnika.SelectedItem = k.tipKorisnika;
                    }

                btnRegistrujSe.Content = "Izmeni";
            }

        }

        private void btnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnRegistrujSe_Click(object sender, RoutedEventArgs e)
        {
            //public Korisnik(string ime, string prezime, string email, string adresaStanovanja, string pol, 
            //string korisnickoIme, string lozinka, string tipKorisnika)

            string ime = tbIme.Text;
            string prezime = tbPrezime.Text;
            string email = tbEmail.Text;
            string adresaStanovanja = tbAdresaStanovanja.Text;

            string pol = "muski";
            if (rbZenski.IsChecked == true)
                pol = "zenski";

            string korisnickoIme = tbKorisnickoIme.Text;
            string lozinka = pbLozinka.Password;
            string tipKorisnika = cbTipKorisnika.SelectedItem.ToString();

            if (upisIzmena == "upis")
            {
                if (!postojiKorisnik(korisnickoIme))
                {

                    Korisnik k = new Korisnik(ime, prezime, email, adresaStanovanja, pol, korisnickoIme, lozinka, tipKorisnika);
                    Podaci.lstKorisnici.Add(k);
                    this.Close();
                }
                else {
                    MessageBox.Show("Uneto korisnicko ime vec postoji!");
                }
            }
            else if (upisIzmena == "izmena") {

                if (!postojiKorisnik(korisnickoIme)) {

                    for (int i = 0; i < Podaci.lstKorisnici.Count; i++) {
                        if (Podaci.lstKorisnici.ElementAt(i).korisnickoIme == korisnickoIme) {

                            Podaci.lstKorisnici.ElementAt(i).ime = ime;
                            Podaci.lstKorisnici.ElementAt(i).prezime = prezime;
                            Podaci.lstKorisnici.ElementAt(i).email = email;
                            Podaci.lstKorisnici.ElementAt(i).adresaStanovanja = adresaStanovanja;
                            Podaci.lstKorisnici.ElementAt(i).pol = pol;
                            Podaci.lstKorisnici.ElementAt(i).korisnickoIme = korisnickoIme;
                            Podaci.lstKorisnici.ElementAt(i).lozinka = lozinka;
                            Podaci.lstKorisnici.ElementAt(i).tipKorisnika = tipKorisnika;

                            this.Close();

                        }

                    }
                }
                else
                {
                    MessageBox.Show("Uneto korisnicko ime vec postoji!");
                }
            }


        }

        private bool postojiKorisnik(string korisnickoIme) {

            foreach (Korisnik k in Podaci.lstKorisnici) {
                if (k.korisnickoIme == korisnickoIme && korisnickoIme!=this.korisnickoIme)
                    //korisnik moze ostaviti korisnicko ime nepromenjeno a ono svakako postoji u listi
                    //treba pronaci da li u  listi postoji korisnicko ime prosledjeno s forme
                    //a da se razlikuje od korisnickog imena prosledjenog formi prilikom ucitavannja za izmenu
                    return true;
            }
            return false;
        }
    }
}
