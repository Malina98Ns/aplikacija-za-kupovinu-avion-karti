﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Projekat
{
   
    public partial class KartaWindow : Window
    {
        private CollectionViewSource cvs;
        public KartaWindow()
        {
            InitializeComponent();
            dataGridKarte.ItemsSource = Podaci.lstKarta;
            cvs = new CollectionViewSource();
            cvs.Source = Podaci.lstKarta;
            dataGridKarte.ItemsSource = cvs.View;
        }

        private void BtnDodaj_Click(object sender, RoutedEventArgs e)
        {
            KartaEditWindow kew = new KartaEditWindow("upis", "");
            kew.ShowDialog();
        }

        private void BtnIzmeni_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridKarte.SelectedIndex != -1 && Podaci.lstKarta.Count > 0)
            {

                Karta k = (Karta)dataGridKarte.SelectedItem;

                KartaEditWindow kew = new KartaEditWindow("izmena", k.brLeta);
                kew.ShowDialog();

                dataGridKarte.Items.Refresh();

            }
        }

        private void BtnObrisi_Click(object sender, RoutedEventArgs e)
        {
            if (dataGridKarte.SelectedIndex != -1 && Podaci.lstKarta.Count > 0)
            {

                Karta k = (Karta)dataGridKarte.SelectedItem;

                PotvrdaBrisanjeWindow pbw = new PotvrdaBrisanjeWindow();
                pbw.ShowDialog();

                if (pbw.DialogResult.Value == true)
                {
                    for (int i = 0; i < Podaci.lstKarta.Count; i++)
                    {
                        if (Podaci.lstKarta.ElementAt(i).brLeta == k.brLeta)
                            Podaci.lstKarta.ElementAt(i).ponistena = true;
                    }
                }

                dataGridKarte.Items.Refresh();
            }
        }
        private void tbPretragaBrLeta_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterBrLeta);
        }

        private void tbPretragaKlase_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterKlasa);
        }

       
        private void tbNazivaPutnika_TextChanged(object sender, TextChangedEventArgs e)
        {
            cvs.Filter += new FilterEventHandler(MyFilterNazivPutnika);
        }
        private void MyFilterBrLeta(object sender, FilterEventArgs e)
        {
            Karta k = e.Item as Karta;
            if (k != null)
            {
                e.Accepted = k.brLeta.ToLower().Contains(tbPretragaPoBrLeta.Text.ToLower());
            }
        }

        private void MyFilterKlasa(object sender, FilterEventArgs e)
        {
            Karta k = e.Item as Karta;
            if (k != null)
            {
                e.Accepted = k.klasaSedista.ToLower().Contains(tbPretragaPoKlasi.Text.ToLower());
            }
        }
        private void MyFilterNazivPutnika(object sender, FilterEventArgs e)
        {
            Karta k = e.Item as Karta;
            if (k != null)
            {
                e.Accepted = k.nazivPutnika.ToLower().Contains(tbPretragaPoNazivuPutnika.Text.ToLower());
            }
        }
        

            private void BtnZatvori_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
